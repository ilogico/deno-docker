#!/bin/sh

LATEST_VERSION=$(wget -qO - https://github.com/denoland/deno/releases | grep 'download/.*/deno-x86_64.unknown-linux-gnu.zip' | sed -E 's#.*download/v(.*)/deno-x86_64-unknown-linux-gnu\.zip.*#\1#' | head -n 1)

if docker pull "ilogico/deno:$LATEST_VERSION" > /dev/null 2>&1; then
  echo "version $LATEST_VERSION already exists"
  exit 0
fi

wget -nv -O deno.zip "https://github.com/denoland/deno/releases/download/v$LATEST_VERSION/deno-x86_64-unknown-linux-gnu.zip"
unzip -o deno.zip
# chmod +x deno
docker build -t "ilogico/deno:$LATEST_VERSION" .
docker tag "ilogico/deno:$LATEST_VERSION" "ilogico/deno:latest"
docker push "ilogico/deno:$LATEST_VERSION"
docker push "ilogico/deno:latest"
